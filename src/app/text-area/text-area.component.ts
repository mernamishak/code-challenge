import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {JsonHandlerService} from "../service/jsonHandler.service";
import {Model} from "../models/formField.model";
import FormModel = Model.FormModel;

@Component({
    selector: 'text-area',
    templateUrl: './text-area.component.html',
    styleUrls: ['./text-area.component.scss']
})
export class TextAreaComponent implements OnInit {
    formModel;
    textAreaInput;
    @Output()
    emitTextArea = new EventEmitter<FormModel>();

    constructor(private jsonHandler: JsonHandlerService) {
    }

    ngOnInit() {
    }

    getJson() {
        if (this.textAreaInput !== '') {
            this.formModel = this.jsonHandler.handleFormFields(this.textAreaInput);
            this.emitTextArea.emit(this.formModel)
        }
    }

   reset(){
       this.textAreaInput='';
       this.emitTextArea.emit(undefined)
;   }
}
